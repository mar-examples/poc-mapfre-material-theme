# PocMapfreMaterial

This project is a POC of use [Mapfre Material Theme](https://bitbucket.org/mar-examples/mapfre-material-theme/src/master/) for Angular applications. Check it out to get more info about theme.

## Get dependencies

Since `mapfre-material-theme` package is published on Azure Artifacts (_snapshots_ feed at the moment), you will need to add rigth NPM registy to get dependencies. To do that:

1. Authenticate your client against Azure Artifacts snapshots feed [Azure .npmrc config](https://docs.microsoft.com/es-es/azure/devops/artifacts/npm/npmrc?view=azure-devops). You may need to install `vsts-npm-auth` helper.
  ```
  npm install -g vsts-npm-auth --registry https://registry.npmjs.com --always-auth false
  ``` 
2. Configure registry file creating an .npmrc file into your project folder with this content:
  ```
  registry=https://pkgs.dev.azure.com/devopsmapfre/devopsmapfre/_packaging/snapshots/npm/registry/ 
  always-auth=true
  ``` 
3. Get an access token (if you have already authenticated before and token has not expired yet, you don't need to do this)
  ```
  vsts-npm-auth -config .npmrc
  ```
4. Resolve and install dependencies
  ```
  npm i
  ```
   
This will resolve and download every project depedencies (included `mapfre-material-theme`)

**Note:** If you just want to install `mapfre-material-theme` into your own application run:
```
npm i --save mapfre-material-theme
```

## Apply theme (not needed in this project)

This will be not necessary in this project, but if you want to use the theme into another project and you have already download `mapfre-material-theme` from Azure Artifacts you will need to tell Angular where the theme is. To do that add relative route of `.css` dist file into Angular.json styles array:
```javascript
  "styles": [
              "node_modules/mapfre-material-theme/dist/mapfre-mat-theme.css",
              "src/styles.css"
            ],
```
Now, you are ready to use the theme in your own application.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
