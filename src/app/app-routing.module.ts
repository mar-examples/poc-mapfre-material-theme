import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardsExampleComponent } from './cards-example/cards-example.component'
import { HomeComponent } from './home/home.component';
import { TableExampleComponent } from './table-example/table-example.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'cards', component: CardsExampleComponent },
  { path: 'table', component: TableExampleComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
